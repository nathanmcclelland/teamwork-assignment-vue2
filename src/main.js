import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')

App.config.globalProperties.$filters = {
  capitalise(value) {
      return value.charAt(0).toUpperCase() + value.substr(1).toLowerCase();
  }
}
